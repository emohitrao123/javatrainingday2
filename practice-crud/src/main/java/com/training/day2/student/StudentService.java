package com.training.day2.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentService {

    @Autowired
    private StudentDao studentDao;

    @GetMapping("/students")
    public List<Student> findAll(){
        return studentDao.enlistAll();
    }
}
