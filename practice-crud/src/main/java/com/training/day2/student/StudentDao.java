package com.training.day2.student;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Component
public class StudentDao {

    private static List<Student> students = new ArrayList<>();

    static {
        students.add(new Student(1, "Mohit","Alpha"));
        students.add(new Student(2, "Pratik","Beta"));
        students.add(new Student(3, "Yash","Gamma"));
    }

    //a method to enlist all the students

    public List<Student> enlistAll(){
        return students;
    }

    //a method to read(or find) one student on the basis of id
    //a method to add a student to the list
}
